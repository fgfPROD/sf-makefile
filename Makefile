include .env
.SILENT:
.DEFAULT_GOAL: help

#---VARIABLES---------------------------------#
#---DOCKER---#
DOCKER = docker
DOCKER_RUN = $(DOCKER) run
DOCKER_EXEC = $(DOCKER) exec
DOCKER_EXEC_WWW = $(DOCKER_EXEC) -w /var/www/project www_${PROJECT}
DOCKER_COMPOSE = docker compose
DOCKER_COMPOSE_UP = $(DOCKER_COMPOSE) up -d
DOCKER_COMPOSE_STOP = $(DOCKER_COMPOSE) stop
#------------#

#---TOOLS---#
PHP = $(DOCKER_EXEC_WWW) php
COMPOSER = $(DOCKER_EXEC_WWW) composer
NPM = cd project/ && npm
SYMFONY = $(DOCKER_EXEC_WWW) symfony
SYMFONY_L = cd project/ && symfony
#------------#

#---SYMFONY--#
SYMFONY_SERVER_START = $(SYMFONY) serve -d
SYMFONY_SERVER_STOP = $(SYMFONY) server:stop
SYMFONY_CONSOLE = $(SYMFONY) console
SYMFONY_CONSOLE_L = $(SYMFONY_L) console
SYMFONY_LINT = $(SYMFONY_CONSOLE) lint:
#------------#

#---COMPOSER-#
COMPOSER_INSTALL = $(COMPOSER) install
COMPOSER_UPDATE = $(COMPOSER) update
#------------#

#---NPM-----#
NPM_INIT = $(NPM) init
NPM_INSTALL = $(NPM) install --force
NPM_UPDATE = $(NPM) update
NPM_BUILD = $(NPM) run build
NPM_DEV = $(NPM) run dev
NPM_WATCH = $(NPM) run watch
#------------#

#---PHPQA---#
PHPQA = jakzal/phpqa
PHPQA_RUN = $(DOCKER_RUN) --init -it --rm -v $(PWD):/project -w /project $(PHPQA)
#------------#

#---PHPUNIT-#
PHPUNIT = APP_ENV=test $(SYMFONY) php bin/phpunit
#------------#

#---COLORS-#
COM_COLOR   = \033[0;34m
OBJ_COLOR   = \033[0;36m
OK_COLOR    = \033[0;32m
ERROR_COLOR = \033[0;31m
WARN_COLOR  = \033[0;33m
NO_COLOR    = \033[m
#------------#
#---------------------------------------------#


##——— 🛠️  HELP —————————————————————————————————————————————————————————————————————————————————————
help: ## Show this help.
	@echo "Symfony-And-Docker-Makefile"
	@echo "---------------------------"
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(firstword $(MAKEFILE_LIST)) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
#---------------------------------------------#

##——— 🐋  DOCKER ——————————————————————————————————————————————————————————————————————————————————
docker-up: ## Start docker containers.
	$(DOCKER_COMPOSE_UP)
.PHONY: docker-up

docker-stop: ## Stop docker containers.
	$(DOCKER_COMPOSE_STOP)
.PHONY: docker-stop
#---------------------------------------------#

##——— 🧠  SYMFONY —————————————————————————————————————————————————————————————————————————————————
sf-check-requirements: ## Check requirements.
	$(DOCKER_EXEC) www_${PROJECT} symfony check:requirements
.PHONY: sf-check-requirements

sf-install: ## Install Symfony
	$(MAKE) -s sf-check-requirements
	echo "$(WARN_COLOR)git config --global user.email ${EMAIL}$(NO_COLOR)"
	echo "$(WARN_COLOR)git config --global user.name ${NAME}$(NO_COLOR)"
	$(DOCKER_EXEC) www_${PROJECT} symfony new project --version="6.1.*" --webapp
#	$(DOCKER_EXEC) www_${PROJECT} composer create-project symfony/website-skeleton project
	echo ""
	echo "$(OK_COLOR)-------------------------------------------------------------------------------------$(NO_COLOR)"
	echo "$(OK_COLOR)-                              SYMFONY INSTALL COMPLETE                             -$(NO_COLOR)"
	echo "$(OK_COLOR)-------------------------------------------------------------------------------------$(NO_COLOR)"
	echo "Go to $(OK_COLOR)http://${HOST}:${PORT}$(NO_COLOR) for check install"
	echo "Go to $(OK_COLOR)http://${HOST}:${DB_PORT}$(NO_COLOR) for check PMA - phpMyAdmin"
	echo "Configure $(WARN_COLOR)DATABASE_URL$(NO_COLOR) on your $(OK_COLOR).env$(NO_COLOR) file ( is not necessary if use $(WARN_COLOR).env.local$(NO_COLOR) )"
	echo "$(OK_COLOR)-------------------------------------------------------------------------------------$(NO_COLOR)"
	$(MAKE) -s sf-env-local
	echo "$(OK_COLOR)-------------------------------------------------------------------------------------$(NO_COLOR)"
	echo ""
.PHONY: sf-install

sf-env-local: # Make .env.local with DATABASE_URL=""
	$(DOCKER_EXEC_WWW) echo 'DATABASE_URL=${DB_URL}' >> ./project/.env.local
	echo "$(WARN_COLOR)"'DATABASE_URL=${DB_URL}'"$(NO_COLOR)"
	echo "The file $(OK_COLOR).env.local$(NO_COLOR) has succeful added."
.PHONY: sf-env-local
##—————————————————————————————————————————————————————————————————————————————————————————————————
sf: ## List of All Symfony commands (make sf command="commande-name")
	$(SYMFONY_CONSOLE) $(command)
.PHONY: sf

##——— Server local ———
sf-start: ## Start symfony server
	$(SYMFONY_SERVER_START)
.PHONY: sf-start

sf-stop: ## Stop symfony server
	$(SYMFONY_SERVER_STOP)
.PHONY: sf-stop

sf-open: ## Open project in a browser
	$(SYMFONY) open:local
.PHONY: sf-open

sf-open-email: ## Open Email catcher
	$(SYMFONY) open:local:webmail
.PHONY: sf-open-email

##——— Command DataBase ———
sf-dc: ## Create symfony database
	$(SYMFONY_CONSOLE) doctrine:database:create --if-not-exists
.PHONY: sf-dc

sf-dd: ## Drop symfony database
	$(SYMFONY_CONSOLE) doctrine:database:drop --if-exists --force
.PHONY: sf-dd

sf-su: ## Update symfony schema database
	$(SYMFONY_CONSOLE) doctrine:schema:update --force
.PHONY: sf-su

sf-mm: ## Make migrations
	$(SYMFONY_CONSOLE) make:migration
.PHONY: sf-mm

sf-dmm: ## Migrate
	$(SYMFONY_CONSOLE) doctrine:migrations:migrate --no-interaction
.PHONY: sf-dmm

sf-fixtures: ## Load fixtures
	$(SYMFONY_CONSOLE) doctrine:fixtures:load --no-interaction
.PHONY: sf-fixtures

##——— Commande Make ———
sf-me: ## Make symfony entity
	$(SYMFONY_CONSOLE_L) make:entity
.PHONY: sf-me

sf-mc: ## Make symfony controller
	$(SYMFONY_CONSOLE_L) make:controller 
#	$(SYMFONY_CONSOLE) make:controller
.PHONY: sf-mc

sf-mf: ## Make symfony Form
	$(SYMFONY_CONSOLE_L) make:form
.PHONY: sf-mf

##——— Command Tools ———

sf-cc: ## Clear symfony cache
	$(SYMFONY_CONSOLE) cache:clear
.PHONY: sf-cc

sf-log: ## Show symfony logs
	$(SYMFONY) server:log
.PHONY: sf-log

sf-perm: ## Fix permissions
	$(DOCKER_EXEC_WWW) chmod -R 777 var
.PHONY: sf-perm

sf-sudo-perm: ## Fix permissions with sudo
	$(DOCKER_EXEC_WWW) sudo chmod -R 777 var
.PHONY: sf-sudo-perm

##——— Commande Dump ———
sf-dump-env: ## Dump env
	$(SYMFONY_CONSOLE) debug:dotenv
.PHONY: sf-dump-env

sf-dump-env-container: ## Dump Env container
	$(SYMFONY_CONSOLE) debug:container --env-vars
.PHONY: sf-dump-env-container

sf-dump-routes: ## Dump routes
	$(SYMFONY_CONSOLE) debug:router
.PHONY: sf-dump-routes
#---------------------------------------------#

##——— 📦  COMPOSER ————————————————————————————————————————————————————————————————————————————————
composer-install: ## Install composer dependencies
	$(COMPOSER_INSTALL)
#	$(MAKE) -s composer-req
.PHONY: composer-install

composer-update: ## Update composer dependencies
	$(COMPOSER_UPDATE)
.PHONY: composer-update

composer-validate: ## Validate composer.json file
	$(COMPOSER) validate
.PHONY: composer-validate

composer-validate-deep: ## Validate composer.json and composer.lock files in strict mode
	$(COMPOSER) validate --strict --check-lock
.PHONY: composer-validate-deep

composer-req: ## Requiere package
	$(COMPOSER) req symfony/webpack-encore-bundle cocur/slugify vich/uploader-bundle easycorp/easyadmin-bundle
	$(COMPOSER) req --dev orm-fixtures fakerphp/faker
.PHONY: composer-req
#---------------------------------------------#

##——— 🐈  NPM —————————————————————————————————————————————————————————————————————————————————————
# npm-init:
# 	$(NPM_INIT)
# 	$(DOCKER_EXEC_WWW) cp package-lock.json package.json
# .PHONY: npm-init

npm-install: ## Install npm dependencies
	[ -f ./project/package.json ] && $(NPM_INSTALL) || echo "$(OK_COLOR)SKIP NPM INSTALL$(NO_COLOR) => File $(WARN_COLOR)package.json$(NO_COLOR) not exiting."
.PHONY: npm-install

npm-update: ## Update npm dependencies
	$(NPM_UPDATE)
.PHONY: npm-update

npm-build: ## Build assets
	[ -f ./project/package.json ] && $(NPM_BUILD) || echo "$(OK_COLOR)SKIP NPM BUILD$(NO_COLOR) => File $(WARN_COLOR)package.json$(NO_COLOR) not exiting."
.PHONY: npm-build

npm-dev: ## Build assets in dev mode
	$(NPM_DEV)
.PHONY: npm-dev

npm-watch: ## Watch assets
	$(NPM_WATCH)
.PHONY: npm-watch

npm-req: ## Get Pack GoodWay
	$(NPM_INSTALL) sass-loader@^13.0.0 sass --save-dev
.PHONY: npm-req
#---------------------------------------------#

##——— 🐛  PHPQA ———————————————————————————————————————————————————————————————————————————————————
qa-cs-fixer-dry-run: ## Run php-cs-fixer in dry-run mode.
	$(PHPQA_RUN) php-cs-fixer fix ./src --rules=@Symfony --verbose --dry-run
.PHONY: qa-cs-fixer-dry-run

qa-cs-fixer: ## Run php-cs-fixer.
	$(PHPQA_RUN) php-cs-fixer fix ./src --rules=@Symfony --verbose
.PHONY: qa-cs-fixer

qa-phpstan: ## Run phpstan.
	$(PHPQA_RUN) phpstan analyse ./src --level=7
.PHONY: qa-phpstan

qa-security-checker: ## Run security-checker.
	$(SYMFONY) security:check
.PHONY: qa-security-checker

qa-phpcpd: ## Run phpcpd (copy/paste detector).
	$(PHPQA_RUN) phpcpd ./src
.PHONY: qa-phpcpd

qa-php-metrics: ## Run php-metrics.
	$(PHPQA_RUN) phpmetrics --report-html=var/phpmetrics ./src
.PHONY: qa-php-metrics

qa-lint-twigs: ## Lint twig files.
	$(SYMFONY_LINT)twig ./templates
.PHONY: qa-lint-twigs

qa-lint-yaml: ## Lint yaml files.
	$(SYMFONY_LINT)yaml ./config
.PHONY: qa-lint-yaml

qa-lint-container: ## Lint container.
	$(SYMFONY_LINT)container
.PHONY: qa-lint-container

qa-lint-schema: ## Lint Doctrine schema.
	$(SYMFONY_CONSOLE) doctrine:schema:validate --skip-sync -vvv --no-interaction
.PHONY: qa-lint-schema

qa-audit: ## Run composer audit.
	$(COMPOSER) audit
.PHONY: qa-audit
#---------------------------------------------#

##——— 🔎  TESTS ———————————————————————————————————————————————————————————————————————————————————
tests: ## Run tests.
	$(PHPUNIT) --testdox
.PHONY: tests

tests-coverage: ## Run tests with coverage.
	$(PHPUNIT) --coverage-html var/coverage
.PHONY: tests-coverage
#---------------------------------------------#

##——— ⭐  OTHERS ——————————————————————————————————————————————————————————————————————————————————
before-commit: qa-cs-fixer qa-phpstan qa-security-checker qa-phpcpd qa-lint-twigs qa-lint-yaml qa-lint-container qa-lint-schema tests ## Run before commit.
.PHONY: before-commit

#first-install: docker-up sf-install composer-install npm-install npm-build sf-perm sf-dc sf-dmm sf-start sf-open ## First install.
first-install: docker-up sf-install composer-install npm-install npm-build sf-perm sf-dc## First install.
.PHONY: first-install

start: docker-up sf-start sf-open ## Start project.
.PHONY: start

stop: docker-stop sf-stop ## Stop project.
.PHONY: stop

reset-db: ## Reset database.
	$(eval CONFIRM := $(shell read -p "Are you sure you want to reset the database? [y/N] " CONFIRM && echo $${CONFIRM:-N}))
	@if [ "$(CONFIRM)" = "y" ]; then \
		$(MAKE) sf-dd; \
		$(MAKE) sf-dc; \
		$(MAKE) sf-dmm; \
	fi
.PHONY: reset-db
#---------------------------------------------#