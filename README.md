# sf-makefile - Symfony Makefile

This Makefile is a **big shortcut** for installing and initialing a **Symfony** project with **Docker**

## See possibilities

```
————————————————————————————————————————————————————————————————————————————————————————————— 
—— 🔥 App ——
init                           Init the project => docker-start composer-install npm-install
—————————————————————————————————————————————————————————————————————————————————————————————
—— 🐳 Docker ——
docker-start                   Start Docker
docker-stop                    Stop Docker
docker-www                     Connexion to www container Docker
—————————————————————————————————————————————————————————————————————————————————————————————
—— 🎻 Composer ——
composer-install               Install dependencies
composer-update                Update dependencies
composer-req                   Requiere package
—— 🐈 NPM ———————————————————————————————————————————————————————————————————————————————————
npm-install                    Install all npm dependencies
npm-update                     Update all npm dependencies
npm-watch                      Update all npm dependencies
npm-tailwindcss                Install Tailwind CSS
—————————————————————————————————————————————————————————————————————————————————————————————
—— 🧠 Symfony ——
sf-install                     Install Symfony
sf-c                           $(PHP) bin/console <command>
—— 🛠️  Symfony Tools ——     
sf-e                           make:entity
sf-cc                          cache:clear
—— 📊 Symfony Database ——    
database-init                  Init database
database-drop                  Create database
database-create                Create database
database-remove                Drop database
database-migration             Make migration
migration                      Alias : database-migration
database-migrate               Migrate migrations
migrate                        Alias : database-migrate
database-fixtures-load         Load fixtures
fixtures                       Alias : database-fixtures-load
—————————————————————————————————————————————————————————————————————————————————————————————
help                           List of commands
—————————————————————————————————————————————————————————————————————————————————————————————
```

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/fgfPROD/sf-makefile.git
git branch -M main
git push -uf origin main
```
